// src/database.rs
use mysql::{OptsBuilder, Pool, PooledConn};
use lazy_static::lazy_static;

// Define the global pool
lazy_static! {
    static ref POOL: Pool = {
        let opts = OptsBuilder::new()
            .ip_or_hostname(Some("localhost"))
            .user(Some("root"))
            .pass(Some("Bxk180621@"))
            .db_name(Some("Dove"))
            .tcp_port(3306)
            .init(vec!["SET time_zone = '+00:00'"]);
        Pool::new(opts).unwrap()
    };
}

pub async fn get_pool() -> PooledConn {
    // Return a reference to the global pool
    POOL.to_owned().get_conn().unwrap()
}

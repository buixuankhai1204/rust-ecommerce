pub mod http;
mod component;

use warp::{http as http_warp, Filter};
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;
use serde::{Serialize, Deserialize};
use crate::http::{setup};
use mysql::{Pool};
mod database;

type Items = HashMap<String, i32>;
#[tokio::main(flavor = "current_thread")]
async fn main() {
    
    let user_routes = setup();
    let routers = user_routes.with(warp::cors().allow_any_origin());
    warp::serve(routers)
        .run(([127, 0, 0, 1], 3030))
        .await;
}

use std::collections::BTreeMap;

use hmac::{Hmac, Mac};
use jwt::{AlgorithmType, Error, Header, SignWithKey, Token, VerifyWithKey};
use sha2::Sha256;
use warp::{reject::Rejection, Filter};

#[derive(Debug)]
pub struct metadata {
    // user_id: String,
    username: String,
}

impl Default for metadata {
    fn default() -> Self {
        metadata {
            // user_id: "".to_string(),
            username: "".to_string(),
        }
    }
}

pub fn parse_header() -> impl Filter<Extract = (Option<metadata>,), Error = Rejection> + Copy {
    let token = warp::header::header::<String>("Authorization");
    token.then(|mut token: String| async move {
        let token_str = token.split_off(7);
        let key: Hmac<Sha256> = Hmac::new_from_slice(b"some-secret").unwrap();
        let claims: Result<BTreeMap<String, String>, Error> = token_str.verify_with_key(&key);

        if claims.is_err() {
            return None;
        } else {
            if claims.as_ref().unwrap().get("username").is_none() {
                return None;
            }

            return Some(metadata {
                // user_id: claims.as_ref().unwrap().clone().get("user_id").as_ref().unwrap().to_string(),
                username: claims
                    .unwrap_or_default()
                    .get("username")
                    .unwrap()
                    .to_string(),
            });
        }
    })
}
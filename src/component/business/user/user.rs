use crate::component::storage::{
    get_all_user, get_user_by_username, insert_user, update_user, UserAccount,
};
use crate::component::business::helper::parse_header;
use crate::component::Utilities::{check_password, hash_password};
use crate::http::{loginRequest, RegisterRequest, UpdateRequest};
use hmac::{Hmac, Mac};
use jwt::{AlgorithmType, Header, SignWithKey, Token, VerifyWithKey};
use sha2::{Sha256, Sha384};
use std::collections::BTreeMap;
pub async fn login<'a>(
    username_request: &'a str,
    password_request: &'a str,
) -> (String, Option<UserAccount>, String) {
    let (err, result) = get_user_by_username(username_request).await;

    if result.is_none() == true {
        return ("account invalidzzzz".parse().unwrap(), None, "".to_string());
    }
    let result = result.unwrap();
    let password = &result.password;

    let (err, check_password) = check_password( password_request, &password);
    if check_password == false {
        return (err.parse().unwrap(), None, "".to_string());
    }

    let key: Hmac<Sha256> = Hmac::new_from_slice(b"some-secret").unwrap();
    let mut claims = BTreeMap::new();
    claims.insert("userame", username_request);
    let token_str = claims.sign_with_key(&key).unwrap();  

    return ("".parse().unwrap(), Some(result), token_str);
}

pub async fn register<'a>(register: RegisterRequest) -> (String, Option<UserAccount>, String) {
    let password = &register.password;
    let username = &register.username;

    let password = hash_password(&password);
    let register_request = RegisterRequest{
        password,
        username: register.username.to_owned(),
        name: register.name,
        email: register.email,
        address: register.address,
        phone_number: register.phone_number,
        profile_picture: register.profile_picture,
    };

    let key: Hmac<Sha256> = Hmac::new_from_slice(b"some-secret").unwrap();
    let mut claims = BTreeMap::new();
    
    claims.insert("username", &username);

    let token_str = claims.sign_with_key(&key).unwrap();
    
    let (err, result) = insert_user(register_request).await;
    if result.is_none() == true {
        return (err.parse().unwrap(), None, String::new());
    }

    

    ("".parse().unwrap(), result, token_str)
}

pub async fn update<'a>(update: UpdateRequest) -> (&'a str, Option<UserAccount>) {

    let (err, result) = update_user(update).await;
    if result.is_none() == true {
        return ("update account failed", None);
    }

    ("", result)
}

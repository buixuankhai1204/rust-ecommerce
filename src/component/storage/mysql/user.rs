use std::error::Error;

use crate::component::storage::mysql::UserAccount;
use crate::database::get_pool;
use crate::http::{loginRequest, RegisterRequest, UpdateRequest};
use mysql::params;
use mysql::prelude::Queryable;
use mysql::{from_row, Pool, Row};

pub async fn get_all_user() -> Vec<UserAccount> {
    let mut conn = get_pool().await;

    let res = conn.query("select * from user").unwrap();
    let mut result: Vec<UserAccount> = Vec::new();
    for re in res.iter() {
        result.push(UserAccount::from(re));
        println!("ok");
    }
    return result;
}
//
// pub async fn get_user_by_id(user_id : i64) -> Option<UserAccount> {
//     let url = "mysql://xuankhai:buixuankhai1204@localhost:3306/schema_name";
//     let pool = Pool::new(url).unwrap();
//     let mut conn = pool.get_conn().unwrap();
//
//     let res = conn
//         .query_first("select * from user where user_id = :user_id")
//         //Unpack Result
//         .map(|row| {
//             //Unpack Option
//             row.map(|(user_id,username, firstname, lastname, email, address, phone_number, birthday,reg_date, is_active)| UserAccount {
//                 user_id: user_id,
//                 username: Some(username),
//                 first_name: Some(firstname),
//                 last_name: Some(lastname),
//                 email: Some(email),
//                 address: Some(address),
//                 phone_number: Some(phone_number),
//                 birthday: Some(birthday),
//                 is_active: is_active,
//                 reg_date: reg_date
//             })
//         });
//     return res.unwrap();
// }

pub async fn get_user_by_username(username: &str) -> (String, Option<UserAccount>) {
    let mut conn = get_pool().await;

    let res = conn
        .exec_first(
            "select * from user where username = :username",
            params! {
              "username"=> username
            },
        )
        .unwrap();

    if res.is_none() == true {
        return ("error".parse().unwrap(), None);
    }
    let result = UserAccount::from(&res.unwrap());
    return ("".parse().unwrap(), Some(result));
}

pub async fn insert_user(user: RegisterRequest) -> (String, Option<UserAccount>) {
    let mut conn = get_pool().await;

    let query = conn.exec_drop(
        "INSERT INTO user (username, password, email, address, name)
      VALUES (:username, :password, :email, :address, :name)",
        params! {
            "username" => &user.username,
            "password" => &user.password,
            "email" => &user.email,
            "address" => &user.address,
            "name" => &user.name,
        },
    )
    .unwrap();
    
    let result = get_user_by_username(&user.username).await;

    (String::from(""), result.1)
}

pub async fn update_user(user: UpdateRequest) -> (String, Option<UserAccount>) {
    let mut conn = get_pool().await;
    let result = conn
    .exec_first(
        "update user set username = :username, email = :email, phone_number = :phone_number, address = :address where username = :username",
        params! {
            "username" => &user.username.unwrap_or_default(),
            "email" => &user.email.unwrap_or_default(),
            "address" => &user.address.unwrap_or_default(),
            "phone_number" => &user.phone_number.unwrap_or_default(),
        }
    ).unwrap();

    if result.is_none() == true {
        return (String::from("update failed"), None);
    }

    let result = UserAccount::from(&result.unwrap());

    (String::from(""), Some(result))
}

use std::{error::Error, time::SystemTime};
use chrono::{Date, NaiveTime};
use mysql::{prelude::FromRow, Value};
use mysql::{from_value, from_value_opt, Row};
use validator::{Validate, ValidationError};
use serde::{Serialize, Deserialize};
use chrono;

#[derive(Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Debug,Serialize,Deserialize)]
pub struct UserAccount {
    pub user_id : u32,
    pub username : String,
    pub fullname : String,
    pub password : String,
    pub email : String,
    pub name : String,
    pub address : String,
    pub profile_picture : String,
    pub bio : String,

}

impl Default for UserAccount {
    fn default() -> Self {
        UserAccount {
            user_id: 0,
            username: "".to_string(),
            fullname: "".to_string(),
            password: "".to_string(),
            email: "".to_string(),
            name: "".to_string(),
            address: "".to_string(),
            profile_picture: "".to_string(),
            bio: "".to_string(),
            
        }
    }
}

#[derive(Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Debug,Serialize,Deserialize)]
pub struct Product {
    pub(crate) id: u64,
    pub(crate) code: String,
    pub(crate) price: u64,
    pub(crate) product_name: String,
}

impl From<&Row> for UserAccount {
    fn from(row : &Row) -> Self {
        let user_id = row.get("userID");
        let username = row.get("username");
        let mut user = UserAccount::default();
        user.user_id = user_id.unwrap();
        user.username = username.unwrap();
        match row.get("profilePicture") {
            val @ Some(Value::NULL) => {
                 user.profile_picture = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.profile_picture = from_value_opt::<String>(val.unwrap()).unwrap()

            }
            _ => {
            }
        }
        match row.get("bio") {
            val @ Some(Value::NULL) => {
                 user.bio = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.bio = from_value_opt::<String>(val.unwrap()).unwrap()

            }
            _ => {
            }
        }

        match row.get("password") {
            val @ Some(Value::NULL) => {
                 user.password = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.password = from_value_opt::<String>(val.unwrap()).unwrap()

            }
            _ => {
            }
        }

        match row.get("name") {
            val @ Some(Value::NULL) => {
                 user.name = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.name = from_value_opt::<String>(val.unwrap()).unwrap()

            }
            _ => {
            }
        }

        match row.get("profilePicture") {
            val @ Some(Value::NULL) => {
                 user.profile_picture = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.profile_picture = from_value_opt::<String>(val.unwrap()).unwrap()
            }
            _ => {
            }
        }
        
        match row.get("address") {
            val @ Some(Value::NULL) => {
                 user.address = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.address = from_value_opt::<String>(val.unwrap()).unwrap()
            }
            _ => {
            }
        }
        
        match row.get("email") {
            val @ Some(Value::NULL) => {
                 user.email = "".to_string();
            },
            val @ Some(Value::Bytes(..)) => {
                user.email = from_value_opt::<String>(val.unwrap()).unwrap()
            }
            _ => {
            }
        }

        user
    }


}
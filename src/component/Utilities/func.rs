use bcrypt::{DEFAULT_COST, hash, verify};

pub fn check_password<'a>(password : &'a str, password_database : &'a str) -> (&'a str,bool){
    let valid = verify(password, password_database).unwrap();
    if valid == false {
        let error = "password in correct!";
        return (error, false);
    }
    return ("",true);
}

pub fn hash_password<'a>(password : &str) -> String{
    let hashed = hash(password, DEFAULT_COST).unwrap();
    return hashed;
}



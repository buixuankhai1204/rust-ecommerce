use mysql::prelude::ToValue;
use crate::component::*;
use crate::http::{loginRequest, RegisterRequest, UpdateRequest};
use validator::Validate;
use crate::http::model::{BodyRespone, UnAuthResponse};

pub async fn login_account(
    login_request : loginRequest,
) -> Result<impl warp::Reply, warp::Rejection>{

    let validtor = login_request.validate();
    if validtor.is_err() == true {
        let error = "invalid type";
        return Ok(warp::reply::json(
            &serde_json::json!({
               "errror" : error
            })
        ));
    }

    let username = login_request.username;
    let password = login_request.password;
    let (error, result, token) = login(&username,&password).await;
    if result.is_none() == true {
        return Ok(warp::reply::json(
            &serde_json::json!({
               "error" : error
            })
        ));
    }
    let result = result.unwrap();

    let json_body = BodyRespone{
        code: 200,
        success: true,
        payload: serde_json::to_value(result).unwrap(),
        token: Some(serde_json::to_value(token).unwrap())

    };
    Ok(warp::reply::json(
        &json_body
    ))
}

pub async fn register_account(
    metadata: Option<metadata>,
    register_request : RegisterRequest,
) -> Result<impl warp::Reply, warp::Rejection>{

    if metadata.is_none() {
        let json_body = UnAuthResponse {
            status: 401,
            message: String::from("Unauthorized")
        };
        return Ok(warp::reply::json(
            &json_body
        ));
    }
    

    let validtor = register_request.validate();
    if validtor.is_err() == true {
        let error = "invalid type";
        return Ok(warp::reply::json(
            &serde_json::json!({
               "errror" : error
            })
        ));
    }

    let (error, result, token) = register(register_request).await;
    if result.is_none() == true {
        return Ok(warp::reply::json(
            &serde_json::json!({
               "errror" : error
            })
        ));
    }
    let result = result.unwrap();

    let json_body = BodyRespone {
        code: 200,
        success: true,
        payload: serde_json::to_value(result).unwrap(),
        token: Some(serde_json::to_value(token).unwrap())
    };
    Ok(warp::reply::json(
        &json_body
    ))
}

pub async fn update_account(
    update_request : UpdateRequest,
) -> Result<impl warp::Reply, warp::Rejection>{

    let validtor = update_request.validate();
    if validtor.is_err() == true {
        let error = "invalid type";
        return Ok(warp::reply::json(
            &serde_json::json!({
               "errror" : error
            })
        ));
    }

    let (error, result) = update(update_request).await;
    if result.is_none() == true {
        return Ok(warp::reply::json(
            &serde_json::json!({
               "errror" : error
            })
        ));
    }
    let result = result.unwrap();

    let json_body = BodyRespone{
        code: 200,
        success: true,
        payload: serde_json::to_value(result).unwrap(),
        token: None
    };
    Ok(warp::reply::json(
        &json_body
    ))
}
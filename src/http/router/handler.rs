use crate::http::{post_json_login, post_json_register, post_json_update};
use warp::{self, filters::BoxedFilter, Filter};
use crate::{wraped_or_tree,wraped_debug_boxed};
use crate::http::service::{login_account, register_account, update_account};


fn login() -> BoxedFilter<(impl warp::Reply,)>  {
    warp::get()
        .and(warp::path!("connect"/"health"))
        .and_then(login_account)
        .boxed()
}

fn register() -> BoxedFilter<(impl warp::Reply,)>  {
    warp::post()
        .and(warp::path!("connect" / "register"))
        .and(post_json_register())
        .and_then(register_account)
        .boxed()
}

fn register() -> BoxedFilter<(impl warp::Reply,)>  {
    warp::post()
        .and(warp::path!("connect" / "register" / usize))
        .and(post_json_register())
        .and_then(register_account)
        .boxed()
}

fn update() -> BoxedFilter<(impl warp::Reply,)>  {
    warp::post()
        .and(warp::path!("connect" / "public"))
        .and(post_json_update())
        .and_then(update_account)
        .boxed()
}




pub fn setup() -> impl Filter<Extract=impl warp::Reply , Error=warp::Rejection> + Clone {
    let user_login_router = login();
    let user_update_router = update();
    let user_register_router = register();

    wraped_or_tree!(
        user_login_router,
        user_update_router,
        user_register_router
      )
}
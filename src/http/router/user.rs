use crate::http::service::{login_account, register_account, update_account};
use crate::http::{post_json_login, post_json_register, post_json_update};
use crate::{wraped_debug_boxed, wraped_or_tree};
use crate::component::parse_header;
use warp::{self, filters::BoxedFilter, Filter};

fn login() -> BoxedFilter<(impl warp::Reply,)> {
    warp::post()
        .and(warp::path!("user" / "login"))
        .and(post_json_login())
        .and_then(login_account)
        .boxed()
}

fn register() -> BoxedFilter<(impl warp::Reply,)> {
    warp::post()
        .and(warp::path!("user" / "register"))
        .and(parse_header())
        .and(post_json_register())
        .and_then( register_account)
        .boxed()
}

fn update() -> BoxedFilter<(impl warp::Reply,)> {
    warp::post()
        .and(warp::path!("user" / "update"))
        .and(post_json_update())
        .and_then(update_account)
        .boxed()
}

pub fn setup() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let user_login_router = login();
    let user_update_router = update();
    let user_register_router = register();

    wraped_or_tree!(user_login_router, user_update_router, user_register_router)
}

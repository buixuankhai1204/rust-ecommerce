use serde::{Serialize, Deserialize};
use warp::Filter;
use validator::{Validate, ValidationError};
use std::sync::Arc;
use serde_json::Value;


#[derive(Debug,Validate,Deserialize,Serialize)]
pub struct BodyRespone {
    pub(crate) code : i32,
    pub(crate) success : bool,
    pub(crate) payload : Value,
    pub (crate) token : Option<Value>,
}

#[derive(Debug,Deserialize,Serialize)]
pub struct UnAuthResponse {
    pub status: i32,
    pub message: String,
}

impl Default for BodyRespone {
    fn default() -> Self {
        BodyRespone{
            code: 0,
            success: false,
            payload: Default::default(),
            token: None,
            
        }
    }
}


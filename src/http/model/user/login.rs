use serde::{Serialize, Deserialize};
use warp::Filter;
use validator::{Validate, ValidationError};
use std::collections::HashMap;
use std::sync::Arc;
use parking_lot::RwLock;

type Items = HashMap<String, i32>;

#[derive(Debug,Validate,Deserialize)]
pub struct loginRequest {
    #[validate(length(min = 1))]
    pub username: String,
    pub password: String,
}

impl Default for loginRequest {
     fn default() -> Self {
        loginRequest{
            username: "".to_string(),
            password: "".to_string()
        }
    }
}

pub fn post_json_login() -> impl Filter<Extract = (loginRequest,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

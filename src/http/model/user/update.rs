use serde::{ Deserialize};
use warp::Filter;
use validator::{Validate};
use std::collections::HashMap;

use utility_types::partial;
type Items = HashMap<String, i32>;

#[derive(Debug,Validate,Deserialize)]
#[partial(PartialUpdateRequest)]
pub struct UpdateRequest {
    #[validate(length(min = 1))]
    pub username: Option<String>,
    pub email: Option<String>,
    pub address: Option<String>,
    pub phone_number: Option<String>,
}

impl Default for UpdateRequest {
    fn default() -> Self {
        UpdateRequest{
            username: None,
            email: None,
            address: None,
            phone_number: None,
        }
    }
}

pub fn post_json_update() -> impl Filter<Extract = (UpdateRequest,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

use serde::{Serialize, Deserialize};
use warp::Filter;
use validator::{Validate, ValidationError};
use std::collections::HashMap;
use std::sync::Arc;
use parking_lot::RwLock;

type Items = HashMap<String, i32>;

#[derive(Debug,Validate,Deserialize)]
pub struct RegisterRequest {
    #[validate(length(min = 1, max = 20))]
    pub username: String,

    #[validate(length(min = 1, max = 20))]
    pub name: String,
    pub password: String,
    #[validate(email)]
    pub email: String,
    pub address: String,
    pub phone_number: String,
    pub profile_picture: String,
}

impl Default for RegisterRequest {
    fn default() -> Self {
        RegisterRequest{
            name: "".to_string(),
            username: "".to_string(),
            password: "".to_string(),
            email: "".to_string(),
            address: "".to_string(),
            phone_number: "".to_string(),
            profile_picture: "".to_string()
        }
    }
}

pub fn post_json_register() -> impl Filter<Extract = (RegisterRequest,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

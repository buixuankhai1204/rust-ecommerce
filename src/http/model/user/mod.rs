pub mod login;
mod register;
mod update;

pub use self::login::{loginRequest, post_json_login};
pub use self::register::{RegisterRequest, post_json_register};
pub use self::update::{UpdateRequest, post_json_update};